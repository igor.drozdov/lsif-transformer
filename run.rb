require 'json'

hover_data = {}
hovers = {}

hover_refs = {}
docs = {}
ranges = {}
doc_ranges = {}
def_refs = {}

File.readlines(ARGV[0]).each do |raw_line|
  line = JSON.parse(raw_line)
  id = line['id']
  label = line['label']

  if label == 'document'
    docs[id] = line['uri']
  end

  if label == 'range'
    start_data, end_data = line.values_at('start', 'end')

    ranges[id] = {
      'loc' => [
        [start_data['line'], end_data['line']],
        [start_data['character'], end_data['character']]
      ]
    }
  end

  if label == 'contains'
    doc_ranges[line['outV']] = line['inVs']
  end

  if label == 'item'
    type = line['property']

    next unless ['definitions', 'references'].include?(type)

    line['inVs'].each do |range_id|
      ranges[range_id]&.tap do |range|
        range['doc_id'] = line['document']
        range['ref_id'] = line['outV']
        range['type'] = line['property']
      end
    end

    if type == 'definitions'
      def_ref = line['inVs'].first
      def_refs[line['outV']] = def_ref if def_ref
    end
  end

  if label == 'hoverResult'
    hover_data[id] = line['result']['contents']
  end

  if label == 'textDocument/hover'
    hovers[line['outV']] = hover_data[line['inV']]
  end

  if label == 'textDocument/references'
    hover_ref = hovers[line['outV']]
    hover_refs[line['inV']] = hover_ref if hover_ref
  end
end

data = {
  'hover_refs' => hover_refs,
  'ranges' => ranges,
  'doc_ranges' => doc_ranges,
  'def_refs' => def_refs,
  'docs' => docs
}

File.open("#{ARGV[0]}.json", 'w') do |f|
  f.write(JSON.dump(data))
end